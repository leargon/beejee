<?php

namespace Beejee\Controllers;

use Beejee\Models\Tasks;
use Beejee\Components\Pagination;

class SiteController
{
	
	public function index($page = 1)
	{
		$tasks = Tasks::getAllTasks($page);
		// Общее количетсво задач (необходимо для постраничной навигации)
        $total = Tasks::getTotalTasks();
        $pagination = new Pagination($total, $page, Tasks::SHOW_BY_DEFAULT, 'page-');
		require_once dirname(__DIR__) . '/Views/Site/index.php';
	}

}