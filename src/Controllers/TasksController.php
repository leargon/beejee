<?php

namespace Beejee\Controllers;

use Beejee\Models\Tasks;

class TasksController
{
	
	public function create()
	{
		if (isset($_POST)) {
			$options['name'] = htmlspecialchars($_POST['name']);
			$options['email'] = htmlspecialchars($_POST['email']);
			$options['text'] = htmlspecialchars($_POST['text']);
			$options['status'] = 0;

			$errors = false;

			if (empty($options['name']) || empty($options['email']) || empty($options['text'])) {
                $errors['name'] = 'Заполните все поля';
            }
            if (!empty($options['name']) && !filter_var($options['email'], FILTER_VALIDATE_EMAIL)) {
                $errors['email'] = 'Неверный формат email';
            }

            if ($errors == false) {
            	if (Tasks::addTask($options)) {
            		$data['code'] = 0;
            	}
            }
            if ($errors) {
            	$data['code'] = 1;
            	$data['error'] = $errors;
            }
            echo json_encode($data);
		}
	}

	// Get data Ajax to edit form
	public function update()
	{
		if ($_POST['action'] === 'edit') {
			$task = Tasks::getTaskById($_POST['id']);
			$data['name'] = $task['name'];
			$data['email'] = $task['email'];
			$data['text'] = $task['text'];
			$data['status'] = $task['status'];
			echo json_encode($data);
		}
	}

	// update task
	public function edit()
	{
		if (isset($_POST)) {
			if (!isset($_SESSION['user'])) {
				$data['code'] = 3;
				echo json_encode($data);
				exit();

			}
			$task = Tasks::getTaskById($_POST['id']);
			$options['name'] = $_POST['name'];
			$options['email'] = $_POST['email'];
			$options['text'] = $_POST['text'];
			$options['status'] = $_POST['status'];
			if ($task['text'] != $options['text']) {
				$options['is_red'] = 1;
			}

			if (Tasks::editTask($_POST['id'],$options)) {
        		$data['code'] = 0;
        	} else {
        		$data['code'] = 1;
        	}

        	echo json_encode($data);
		}
	}

}