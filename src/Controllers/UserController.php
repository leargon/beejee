<?php
namespace Beejee\Controllers;

use Beejee\Models\User;
/**
 * 
 */
class UserController
{
	
	/**
     * Action для страницы "Вход на сайт"
     */
    public function login()
    {
        // Переменные для формы
        $login = false;
        $password = false;

        
        // Обработка формы
        if (isset($_POST)) {
            // Если форма отправлена 
            // Получаем данные из формы
            $login = $_POST['login'];
            $password = $_POST['pass'];

            // Флаг ошибок
            $errors = false;

            if (empty($login) || empty($password)) {
                $errors['name'] = 'Заполните все поля';
            }


            // Проверяем существует ли пользователь
            $userId = User::checkUserData($login, $password);

            //var_dump($userId);

            if ($userId == false) {
                // Если данные неправильные - показываем ошибку
                $errors['login'] = 'Неправильные данные для входа на сайт';
                $data['code'] = 1;
            	$data['error'] = $errors;
            } else {
                // Если данные правильные, запоминаем пользователя (сессия)
                User::auth($userId);
                $data['code'] = 0;
            }
            echo json_encode($data);
        }

    }

    public function logout()
    {
        // Стартуем сессию
        session_start();
        
        // Удаляем информацию о пользователе из сессии
        unset($_SESSION["user"]);
        
        // Перенаправляем пользователя на главную страницу
        header("Location: /");
    }

}