<?php

namespace Beejee\Models;

use Beejee\Components\Db;
use \PDO;

class Tasks
{
    const SHOW_BY_DEFAULT = 3;

	// Получаем все задачи
	public static function getAllTasks($page = 1)
	{
        $limit = Tasks::SHOW_BY_DEFAULT;
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
        $sortBase = 'ORDER BY';
        $sortCol = 'id';
        $sortType = 'ASC';
        if (isset($_GET['sort'])) {
            $sortCol = $_GET['sort'];
            $sortType = $_GET['sortType'];
        }
		$db = Db::getConnection();
		$result = $db->query("SELECT * FROM tasks $sortBase $sortCol $sortType LIMIT $limit OFFSET $offset");
        $taskList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $taskList[$i]['id'] = $row['id'];
            $taskList[$i]['name'] = $row['name'];
            $taskList[$i]['email'] = $row['email'];
            $taskList[$i]['text'] = $row['text'];
            $taskList[$i]['status'] = $row['status'];
            $taskList[$i]['is_red'] = $row['is_red'];
            $i++;
        }
        return $taskList;
	}

	// Получаем задачу по id
	public static function getTaskById($id)
	{

        $db = Db::getConnection();
        $sql = 'SELECT * FROM tasks WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result->fetch();
	}

	// Добавление новой задачи
	public static function addTask($options)
	{
		$db = Db::getConnection();
        $sql = 'INSERT INTO tasks '
                . '(name, email, text, status)'
                . 'VALUES '
                . '(:name, :email, :text, :status)';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':email', $options['email'], PDO::PARAM_STR);
        $result->bindParam(':text', $options['text'], PDO::PARAM_STR);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        if ($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;
	}

	// Редактирование задачи
	public static function editTask($id, $options)
	{
		$db = Db::getConnection();

		$sql = "UPDATE tasks
            SET 
                name = :name, 
                email = :email, 
                text = :text,
                status = :status,
                is_red = :is_red
            WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':email', $options['email'], PDO::PARAM_STR);
        $result->bindParam(':text', $options['text'], PDO::PARAM_STR);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        $result->bindParam(':is_red', $options['is_red'], PDO::PARAM_INT);
        return $result->execute();
	}

    // Получаем количество всех задач
    public static function getTotalTasks()
    {
        $db = Db::getConnection();
        $sql = 'SELECT count(id) AS count FROM tasks';
        $result = $db->prepare($sql);
        $result->bindParam(':category_id', $categoryId, PDO::PARAM_INT);
        $result->execute();
        $row = $result->fetch();
        return $row['count'];
    }

    // Получаем расшифровку для статуса
    public static function getStatusText($status)
    {
        switch ($status) {
            case '1':
                return "<span class=\"badge badge-success\">Выполнено</span>";
                break;
            case '0':
                return "<span class=\"badge badge-primary\">В работе</span>";
                break;
        }
    }

}