<?php

return [
	 ['GET', '/[page-{page:\d+}]', '\SiteController/index'],
	 ['POST', '/login', '\UserController/login'],
	 ['POST', '/logout', '\UserController/logout'],
	 ['POST', '/create', '\TasksController/create'],
	 ['POST', '/update', '\TasksController/update'],
	 ['POST', '/edit', '\TasksController/edit'],
];