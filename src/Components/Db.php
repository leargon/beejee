<?php

namespace Beejee\Components;
use \PDO;
/**
 * 
 */
class Db
{
	
    public static function getConnection()
    {
        // Получаем параметры подключения из файла
        $paramsPath = dirname(__DIR__) . '/Config/Params.php';
        $params = include($paramsPath);

        // Устанавливаем соединение
        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
        $db = new PDO($dsn, $params['user'], $params['password']);

        // Задаем кодировку
        $db->exec("set names utf8");

        return $db;
    }
}