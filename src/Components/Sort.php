<?php
namespace Beejee\Components;
/**
 * 
 */
class Sort
{
	
	public static function sortUri($sort, $sortType = 'desc') {
	  $uri = $_SERVER['REQUEST_URI'];
	  $uri = explode('?', $uri);
	  if (count($uri) > 1)  {
	    parse_str($uri[1], $params);
	    $params['sort'] = $sort;
	    
	    if ($_GET['sortType'] == $sortType) {
	      $sortType = 'asc';
	    }
	    $params['sortType'] = $sortType;
	    $a = '?'.http_build_query($params);
	  } else $a = "?sort=$sort&sortType=$sortType";
	  
	  
	  return $a;
	}
}