<?php 

use Beejee\Components\Sort;
use Beejee\Models\Tasks;

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <!-- Navigation -->
    <div class="bg-light">
      <div class="container pl-0 pr-0">
        <nav class="navbar navbar-light bg-light">
          <a class="navbar-brand" href="/">Задачник</a>
          <!-- <form class="form-inline"> -->
            
            <?php if(isset($_SESSION['user'])): ?>

              <form class="form-inline" method="POST" action="/logout">
                <button class="btn btn-outline-success">Выход</button>
              </form>

            <?php else: ?>
              
              <button class="btn btn-outline-success" type="button"  data-toggle="modal" data-target="#logModal">Вход</button>

            <?php endif; ?>

        </nav>
      </div>
    </div>
    <!-- Navigation end -->

    <!-- Tasks -->
    <div class="tasks">
      <div class="container">
        <h2>Список задач</h2>
        <button type="button" class="btn btn-primary mb-3 create" data-toggle="modal" data-target="#FormModal">Создать задачу</button>
        <div></div>
        <div id="output" class="d-none alert alert-warning alert-dismissible fade show" role="alert">
          <strong>Сообщение добавлено</strong>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="alert alert-danger errors d-none" role="alert"></div>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <!-- <th scope="col">#</th> -->
                <th scope="col">Имя <a href="<?=Sort::sortUri('name')?>"><i class="fas fa-sort"></i></a></th>
                <th scope="col">Email <a href="<?=Sort::sortUri('email')?>"><i class="fas fa-sort"></i></a></th>
                <th scope="col">Текст</th>
                <th scope="col">Статус <a href="<?=Sort::sortUri('status')?>"><i class="fas fa-sort"></i></a></th>
                <?php if(isset($_SESSION['user'])): ?>
                <th scope="col">Операции</th>
                <?php endif; ?>
              </tr>
            </thead>
            <tbody>
              
              <?php foreach($tasks as $task): ?>

                <tr>
                  <!-- <th class="align-middle" scope="row">1</th> -->
                  <td class="align-middle"><?=$task['name']?></td>
                  <td class="align-middle"><?=$task['email']?></td>
                  <td class="align-middle"><?=$task['text']?></td>
                  <td class="align-middle"><?=Tasks::getStatusText($task['status'])?>
                    <?php if($task['is_red'] == 1): ?>
                      <br><span class="badge badge-warning">Отредактировано</span>
                    <?php endif; ?>
                  </td>
                  <?php if(isset($_SESSION['user'])): ?>
                  <td class="align-middle"><button type="button" class="btn btn-warning btn-sm edit"  data-toggle="modal" data-target="#FormModal" data-id="<?=$task['id']?>">Редактировать</button></td>
                  <?php endif; ?>
                </tr>

              <?php endforeach; ?>

              <!-- <tr>
                <th scope="row">1</th>
                <td>Енифер</td>
                <td>enifer@mail.ru</td>
                <td>Текст задачи</td>
                <td><span class="badge badge-primary">В работе</span></td>
                <td><button type="button" class="btn btn-warning btn-sm"  data-toggle="modal" data-target="#FormModal">Редактировать</button></td>
              </tr>
              <tr>
                <th scope="row" class="align-middle">2</th>
                <td class="align-middle">Енифер</td>
                <td class="align-middle">enifer@mail.ru</td>
                <td class="align-middle">Текст задачи</td>
                <td class="align-middle"><span class="badge badge-success">Выполнено</span><br><span class="badge badge-warning">Отредактировано</span></td>
                <td class="align-middle"><button type="button" class="btn btn-warning btn-sm">Редактировать</button></td>
              </tr>
              <tr>
                <th scope="row">3</th>
                <td>Енифер</td>
                <td>enifer@mail.ru</td>
                <td>Текст задачи</td>
                <td><span class="badge badge-primary">В работе</span></td>
                <td><button type="button" class="btn btn-warning btn-sm">Редактировать</button></td>
              </tr> -->
            </tbody>
          </table>
        </div>
        <nav aria-label="Page navigation example">
          <?php echo $pagination->get(); ?>
        </nav>
      </div>
    </div>
    <!-- Tasks end -->

    <!-- Modal Create Task form-->
    <div class="modal fade" id="FormModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Добавить задачу</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="create" method="POST">
              <div class="form-group">
                <label for="name">Имя</label>
                <input type="text" class="form-control" id="name" name="name">
              </div>
              <div class="form-group">
                <label for="email">Почта</label>
                <input type="email" class="form-control" id="email" name="email">
              </div>
              <div class="form-group">
                <label for="text">Текст задачи</label>
                <textarea class="form-control" name="text" id="text"></textarea>
              </div>
              <?php if(isset($_SESSION['user'])): ?>
                <!-- <div class="form-group form-check d-none" id="is_ready">
                  <input type="checkbox" class="form-check-input" name="status">
                  <label class="form-check-label" for="ready">Выполнено</label>
                </div> -->
                <div class="form-group d-none" id="is_ready">
                <label for="text">Выполнено</label>
                  <select class="custom-select" name="status" id="status">
                    <option value="0">Нет</option>
                    <option value="1">Да</option>
                  </select>
                </div>
              <?php endif; ?>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            <button type="submit" name="submit" form="create" class="btn btn-primary submit">Добавить</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal Create Task form end -->

    <!-- Modal Log in form-->
    <div class="modal fade" id="logModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Вход</h5>

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="alert alert-danger log-errors d-none" role="alert"></div>
            <form id="logForm" method="POST">
              <div class="form-group">
                <label for="logEmail">Логин</label>
                <input type="text" class="form-control" id="logEmail" name="login">
              </div>
              <div class="form-group">
                <label for="pass">Пароль</label>
                <input type="password" name="pass" class="form-control" id="pass">
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            <button form="logForm" name="submit" class="btn btn-primary submit">Войти</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal Log in form end -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/64f91110a4.js" crossorigin="anonymous"></script>
    <script src="js/script.js"></script>
  </body>
</html>