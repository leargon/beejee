$(document).ready(function() {
	
      var flag = false;
      var action_url = '/create';

      // Get data for edit form
      $('.edit').on('click', function(){
          var id = $(this).attr('data-id');
          action_url = '/edit';
          $('.submit').html('Внести изменения');
          $('#create').attr('action', '/edit');
          $("#is_ready").removeClass('d-none');
          // $('#create').append('<input type="checkbox" class="form-check-input" name="ready" value="1">');
          $('#create').append('<input type="hidden" class="hidden" name="id" value="' + id + '" />');
          var data = {id : id, action: 'edit'};
          $.ajax({
            type: 'POST',
            url: '/update',
            data: data,
            success: function(response){
              var data = JSON.parse(response);
              $('#name').val(data.name);
              $('#email').val(data.email);
              $('#text').val(data.text);
              $('#status').val(data.status);
            }
          });
      });

      // Change button and delete hidden input when add task
      $('.create').on('click', function(){
        action_url = '/create';
        $('.submit').html('Добавить задачу');
        //$('#create').attr('action', '/create');
        $('.hidden').remove();
        $('.form-control').val('');
      });

      // Ajax handling of task form
      $('#create').on('submit', function(e){
        e.preventDefault();
        var form = $(this);
        var data = form.serialize();
        $(".submit").prop("disabled", true);
        $.ajax({
            type: 'POST',
            url: action_url,
            data: data,
            success: function(response){
              var data = JSON.parse(response);
              $('#FormModal').modal('hide');
              if (data.code == 0) {
                $("#output").removeClass('d-none');
                $(".errors").addClass('d-none');
              }
              if (data.code == 1) {
                var row = "";
                $.each(data.error, function(idx, obj){ 
                  row += '<p>' + obj + '</p>'; 
                });
                $('.errors').html(row);
                $(".errors").removeClass('d-none');
              }
              if (data.code == 3) {
                var row = 'Ошибка авторизации';
                $('.errors').html(row);
                $(".errors").removeClass('d-none');
              }
              $(".submit").prop("disabled", false);
              console.log(response);
            }
          });
      });

      // Ajax login form
      $('#logForm').on('submit', function(e){
        e.preventDefault();
        var form = $(this);
        var data = form.serialize();
        $(".submit").prop("disabled", true);
        $.ajax({
            type: 'POST',
            url: '/login',
            data: data,
            success: function(response){
              var data = JSON.parse(response);
              $('#FormModal').modal('hide');
              if (data.code == 0) {
                // $("#output").removeClass('d-none');
                // $(".errors").addClass('d-none');
                window.location.href = "/";
              }
              if (data.code == 1) {
                var row = "";
                $.each(data.error, function(idx, obj){ 
                  row += '<p>' + obj + '</p>'; 
                });
                $('.log-errors').html(row);
                $(".log-errors").removeClass('d-none');
              }
              $(".submit").prop("disabled", false);
              console.log(response);
            }
          });
      });
});